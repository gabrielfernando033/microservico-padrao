package br.com.grupolibra.api.models.due;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "Despacho.findAll", query = "SELECT d FROM Despacho d")
@Table(name="CMSDESPACHO")
public class Despacho {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "despacho_seq")
	@SequenceGenerator(name = "despacho_seq", sequenceName = "despacho_seq", allocationSize = 1)
	private long id;
	
	@Column(name = "CMS_OBJREF_CONTAINER")
	private BigDecimal objref_container;
	
	@Column(name = "CMS_OBJREF_GRUPOMERCADORIA")
	private BigDecimal objref_grupomercadoria;
	
	@Column(name = "CMS_NRCONTAINER")
	private String nrcontainer;
	
	@Column(name = "CMS_NRODOC")
	private String nrodoc;
	
	@Column(name = "CMS_DATAENTRADA")
	private Date dataentrada;
	
	@Column(name = "SCX_NRODUE")
	private String nrodue;
	
	@Column(name = "SCX_SITUACAODUE")
	private BigDecimal situacaodue;
	
	@Column(name = "SCX_TARA")
	private Integer tara;
	
	@Column(name = "SCX_LACRE")
	private String lacre;
	
	@Column(name = "DESPACHADO")
	private Integer despachado;
	
	@Column(name = "SCX_CODRECINTOADEMBARQUE")
	private String codrecintoadembarque;
	
	@Column(name = "SCX_PESOBRUTO")
	private BigDecimal pesobruto;
	
	@Column(name = "CMS_OBJREF_PREVISAO")
	private BigDecimal objref_previsao;
	
	@Column(name = "CMS_VALOR")
	private BigDecimal valor;
	
	@Column(name = "SCX_OBJREF_CLIENTE")
	private BigDecimal objref_cliente;
	
	@Column(name = "SCX_NUMERODAT")
	private String numerodat;
	
	@Column(name = "SCX_SITUACAODOCUMENTODAT")
	private String situacaodocumentodat;
	
	@Column(name = "SCX_DATASITUACAODUE")
	private Date datasituacaodue;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getObjref_container() {
		return objref_container;
	}

	public void setObjref_container(BigDecimal objref_container) {
		this.objref_container = objref_container;
	}

	public BigDecimal getObjref_grupomercadoria() {
		return objref_grupomercadoria;
	}

	public void setObjref_grupomercadoria(BigDecimal objref_grupomercadoria) {
		this.objref_grupomercadoria = objref_grupomercadoria;
	}

	public String getNrcontainer() {
		return nrcontainer;
	}

	public void setNrcontainer(String nrcontainer) {
		this.nrcontainer = nrcontainer;
	}

	public String getNrodoc() {
		return nrodoc;
	}

	public void setNrodoc(String nrodoc) {
		this.nrodoc = nrodoc;
	}

	public Date getDataentrada() {
		return dataentrada;
	}

	public void setDataentrada(Date dataentrada) {
		this.dataentrada = dataentrada;
	}

	public String getNrodue() {
		return nrodue;
	}

	public void setNrodue(String nrodue) {
		this.nrodue = nrodue;
	}

	public BigDecimal getSituacaodue() {
		return situacaodue;
	}

	public void setSituacaodue(BigDecimal situacaodue) {
		this.situacaodue = situacaodue;
	}

	public Integer getTara() {
		return tara;
	}

	public void setTara(Integer tara) {
		this.tara = tara;
	}

	public String getLacre() {
		return lacre;
	}

	public void setLacre(String lacre) {
		this.lacre = lacre;
	}

	public Integer getDespachado() {
		return despachado;
	}

	public void setDespachado(Integer despachado) {
		this.despachado = despachado;
	}

	public String getCodrecintoadembarque() {
		return codrecintoadembarque;
	}

	public void setCodrecintoadembarque(String codrecintoadembarque) {
		this.codrecintoadembarque = codrecintoadembarque;
	}

	public BigDecimal getPesobruto() {
		return pesobruto;
	}

	public void setPesobruto(BigDecimal pesobruto) {
		this.pesobruto = pesobruto;
	}

	public BigDecimal getObjref_previsao() {
		return objref_previsao;
	}

	public void setObjref_previsao(BigDecimal objref_previsao) {
		this.objref_previsao = objref_previsao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getObjref_cliente() {
		return objref_cliente;
	}

	public void setObjref_cliente(BigDecimal objref_cliente) {
		this.objref_cliente = objref_cliente;
	}

	public String getNumerodat() {
		return numerodat;
	}

	public void setNumerodat(String numerodat) {
		this.numerodat = numerodat;
	}

	public String getSituacaodocumentodat() {
		return situacaodocumentodat;
	}

	public void setSituacaodocumentodat(String situacaodocumentodat) {
		this.situacaodocumentodat = situacaodocumentodat;
	}

	public Date getDatasituacaodue() {
		return datasituacaodue;
	}

	public void setDatasituacaodue(Date datasituacaodue) {
		this.datasituacaodue = datasituacaodue;
	}
	
}