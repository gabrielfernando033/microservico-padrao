package br.com.grupolibra.api.controllers;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.grupolibra.api.models.cms.CMSCliente;
import br.com.grupolibra.api.models.due.Despacho;
import br.com.grupolibra.api.repositories.cms.CMSClienteRepository;
import br.com.grupolibra.api.repositories.due.DespachoRepository;

@RestController
public class PadraoController {
	
	@Autowired
	private CMSClienteRepository cmsClienteRepository;
	
	@Autowired
	private DespachoRepository despachoRepository;
	
	@CrossOrigin
	@RequestMapping(value = "cms/clientes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Object teste() {
		List<CMSCliente> clientes = cmsClienteRepository.findByCnpj("60561719002258");
		return clientes;
	}
	
	@CrossOrigin
	@RequestMapping(value = "cms/cliente", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Object insertCliente() {

		CMSCliente sgipa_cliente = new CMSCliente();
		sgipa_cliente.setObjref(new BigDecimal(373932)); // Atualizar objref para poder inserir
		sgipa_cliente.setNome("Siscomex Teste");
		sgipa_cliente.setCnpj("1231354545454");
		sgipa_cliente.setContato(null);
		sgipa_cliente.setTelefone(null);
		sgipa_cliente.setWfk_usuincl("APIDUE");
		sgipa_cliente.setWfk_dtaincl(new Date(System.currentTimeMillis()));
		sgipa_cliente.setWfk_usualte(null);
		sgipa_cliente.setWfk_dtaalte(null);
		sgipa_cliente.setCodigosom(null);
		sgipa_cliente.setEstado(null);
		sgipa_cliente.setObjref_municipio(null);
		sgipa_cliente.setInscrestadual(null);
		sgipa_cliente.setSituacao(0);
		sgipa_cliente.setCaptacao(null);
		sgipa_cliente.setCep(null);
		sgipa_cliente.setObjref_clientecor(null);
		sgipa_cliente.setReferenciawmscliente(null);
		sgipa_cliente.setReferenciawmsnvocc(null);
		sgipa_cliente.setIntegracaocrm(null);
		sgipa_cliente.setId_sap(null);
		sgipa_cliente.setEmail(null);
		cmsClienteRepository.save(sgipa_cliente);
		
		return "Inseriu";
	}
	
	@CrossOrigin
	@RequestMapping(value = "due/despachos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Object getAllDespachos() {
		List<Despacho> clientes = despachoRepository.findAll();
		return clientes;
	}

}