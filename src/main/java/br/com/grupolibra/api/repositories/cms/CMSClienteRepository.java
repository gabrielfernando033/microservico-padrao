package br.com.grupolibra.api.repositories.cms;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.api.models.cms.CMSCliente;

@Repository
public interface CMSClienteRepository extends CrudRepository<CMSCliente, Long> {
	
	public List<CMSCliente> findAll();
	public List<CMSCliente> findByCnpj(String cnpj);
	
}