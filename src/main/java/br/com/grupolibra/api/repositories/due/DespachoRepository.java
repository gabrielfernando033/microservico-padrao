package br.com.grupolibra.api.repositories.due;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.grupolibra.api.models.due.Despacho;

@Repository
public interface DespachoRepository extends CrudRepository<Despacho, Long> {
	
	public List<Despacho> findAll();
	public List<Despacho> findById(long id);
	public List<Despacho> findByNrcontainerAndNrodoc(String nrcontainer, String nrodoc);
	
	@Query(value = "SELECT * FROM CMSDESPACHO WHERE DESPACHADO = 0", nativeQuery = true)
	public List<Despacho> findNaoDespachado();
	
}